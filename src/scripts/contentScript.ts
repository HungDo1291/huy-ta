'use strict';
import { getDefinitionWord } from '../services/definition';

const getSelectedText = (): string => {
  if (window.getSelection) {
    return window.getSelection()?.toString() || '';
  }
  if (document.getSelection) {
    return document.getSelection()?.toString() || '';
  }

  //For IE < 9
  if ((document as any).selection) {
    return (document as any).selection.createRange().text;
  }

  return '';
};

const getSelectionNode = (): Selection | null => {
  if (window.getSelection) {
    return window.getSelection();
  }
  if (document.getSelection) {
    return document.getSelection();
  }

  //For IE < 9
  if ((document as any).selection) {
    return (document as any).selection.createRange();
  }
  return null;
};

const generateTooltip = (domRect: DOMRect): HTMLElement => {
  const tooltip = document.createElement('div');
  tooltip.id = 'tooltip';
  tooltip.style.position = 'fixed';
  tooltip.style.left = `${domRect.right / 2 + domRect.left / 2}px`;
  tooltip.style.top = `${domRect.bottom}px`;
  tooltip.style.borderRadius = '6px';
  tooltip.style.minWidth = `${domRect.width}px`;
  tooltip.style.maxWidth = '500px';
  tooltip.style.padding = '0px';
  tooltip.style.backgroundColor = 'white';
  tooltip.style.boxShadow = 'black 0px 0px 20px';
  tooltip.style.zIndex = '1000';
  tooltip.style.color = 'rgb(15, 33, 73)';
  tooltip.style.fontSize = '16px';
  return tooltip;
};

const generateIcon = (domRect: DOMRect): HTMLElement => {
  const icon = document.createElement('img');
  icon.id = 'icon';
  icon.style.position = 'fixed';
  icon.style.left = `${domRect.right}px`;
  icon.style.top = `${domRect.bottom}px`;
  icon.style.padding = '5px';
  icon.style.borderRadius = '6px';
  icon.style.backgroundColor = 'lightsteelblue';
  icon.style.maxWidth = '25px';
  icon.style.maxHeight = '25px';
  icon.src =
    'https://cdn.iconscout.com/icon/free/png-256/free-logo-56-96774.png';
  icon.alt = 'definite icon';
  return icon;
};

const generateText = (text: string): string => {
  return `<div style="padding: 10px; font-size:16px; color: rgb(15, 33, 73)">${text}</div>`;
};

const setElement = (element: HTMLElement): void => {
  body?.appendChild(element);
};

const handleClickIcon = async ({
  domRect,
  element,
  selectionText,
}: {
  domRect: DOMRect;
  element: HTMLElement;
  selectionText: string;
}): Promise<void> => {
  const tooltip = generateTooltip(domRect);
  setElement(tooltip);
  element?.remove();
  const tooltipElement = document.querySelector<HTMLDivElement>('div#tooltip');

  tooltipElement!.innerHTML = generateText('Loading...');

  try {
    const data = await getDefinitionWord(selectionText);
    const definitionWords = data?.[0];
    const { shortdef } = definitionWords;

    if (tooltipElement) {
      const definitionString = shortdef
        ?.map((shortDefText: string) => {
          return generateText(shortDefText);
        })
        .join('');

      tooltipElement!.innerHTML =
        definitionString || generateText('Cannot define these words.');
    }
  } catch (error) {
    const errorText = generateText('Cannot define these words.');
    tooltipElement!.innerHTML = errorText;
    console.error(error);
  }
};

const getSelectionRect = (): DOMRect => {
  const selectionNode: Selection | null = getSelectionNode();
  const node = selectionNode!.getRangeAt(0);
  const selectionRect = node.getBoundingClientRect();
  return selectionRect;
};

const handleMouseUp = (): void => {
  const selectionText = getSelectedText()?.trim();
  if (selectionText.length > 0) {
    const selectionRect = getSelectionRect();
    const iconElement = body?.querySelector('img#icon');
    const icon = generateIcon(selectionRect);

    if (!iconElement) {
      setElement(icon);
    }

    icon.addEventListener('click', () =>
      handleClickIcon({
        domRect: selectionRect,
        element: icon,
        selectionText,
      })
    );
  }
};

const handleMouseDown = (event: MouseEvent): void => {
  event.stopPropagation();

  const target = event.target as HTMLElement;
  const iconElement = body?.querySelector('img#icon');
  const tooltipElement = body?.querySelector('div#tooltip');

  if (iconElement && !iconElement.contains(target)) {
    iconElement?.remove();
    return;
  }

  if (tooltipElement) {
    tooltipElement?.remove();
  }
};

const { body } = document;

body?.addEventListener('mouseup', handleMouseUp);
body?.addEventListener('mousedown', handleMouseDown);
