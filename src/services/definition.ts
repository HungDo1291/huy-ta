import axios from 'axios';

const HOST_API =
  'https://www.dictionaryapi.com/api/v3/references/collegiate/json';

const API_KEY = '';

export const getDefinitionWord = async (word: string) => {
  const response = await axios.get(`${HOST_API}/${word}?key=${API_KEY}`);
  return response.data;
};
