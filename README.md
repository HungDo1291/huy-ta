## Getting Started

This readme provides instructions on how to start a Chrome extension project. Follow these steps to set up and run the project.

# Installation

Install packages needed:

### `npm install`

# Start local development

To run this application locally, you will need to add a API_KEY to the definition.ts in services folder.

### `npm run watch`

To run the extension in development mode, execute the following command:

Runs the app in development mode.<br>
Then follow these instructions to see your app:

1. Open **chrome://extensions**
2. Check the **Developer mode** checkbox
3. Click on the **Load unpacked extension** button
4. Select the folder **huy-ta/build**

## Feature

Whenever the user highlights a word, an icon will appear. When the user clicks on that icon, the definition of the word will be displayed.
